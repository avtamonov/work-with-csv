package com.company;

import java.util.List;

public class Entity {

    private String vendorCode;

    private String codeKoshin;

    private String codeChang;

    private String description;

    private String pic;

    private String picKoshin;

    private String picChang;

    private String pdf;

    private String pdfKoshin;

    private String pdfChang;

    private String url;

    private String urlKoshin;

    private String urlChang;

    private String vendor;

    private String species;

    private String speciesKoshin;

    private String speciesChang;

    private String capacity;

    private String voltage;

    private String serial;

    private String size;

    private String sizeChang;

    private String sizeKoshin;

    private String temp;

    private String tempKoshin;

    private String tempChang;

    private String time;

    private String countOfOns;

    private String ChangSerial;

    private String KoshinSerial;

    private String anotherSerial;

    private String anotherSerial1;

    private int maxLengh;

    public Entity() {
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public String getPic() {
        return pic;
    }

    public String getPdf() {
        return pdf;
    }

    public String getUrl() {
        return url;
    }

    public String getVendor() {
        return vendor;
    }

    public String getSpecies() {
        return species;
    }

    public String getCapacity() {
        return capacity;
    }

    public String getVoltage() {
        return voltage;
    }

    public String getSerial() {
        return serial;
    }

    public String getSize() {
        return size;
    }

    public String getTemp() {
        return temp;
    }

    public String getTime() {
        return time;
    }

    public String getCountOfOns() {
        return countOfOns;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setCountOfOns(String countOfOns) {
        this.countOfOns = countOfOns;
    }

    public String getChangSeria() {
        return ChangSerial;
    }

    public String getKoshinSeria() {
        return KoshinSerial;
    }

    public void setChangSeria(String changSeria) {
        ChangSerial = changSeria;
    }

    public void setKoshinSeria(String koshinSeria) {
        KoshinSerial = koshinSeria;
    }

    public void setPicKoshin(String picKoshin) {
        this.picKoshin = picKoshin;
    }

    public void setPicChang(String picChang) {
        this.picChang = picChang;
    }

    public void setPdfKoshin(String pdfKoshin) {
        this.pdfKoshin = pdfKoshin;
    }

    public void setPdfChang(String pdfChang) {
        this.pdfChang = pdfChang;
    }

    public void setUrlKoshin(String urlKoshin) {
        this.urlKoshin = urlKoshin;
    }

    public void setUrlChang(String urlChang) {
        this.urlChang = urlChang;
    }

    public void setTempKoshin(String tempKoshin) {
        this.tempKoshin = tempKoshin;
    }

    public void setTempChang(String tempChang) {
        this.tempChang = tempChang;
    }

    public void setChangSerial(String changSerial) {
        ChangSerial = changSerial;
    }

    public void setKoshinSerial(String koshinSerial) { KoshinSerial = koshinSerial;
    }

    public String getPicKoshin() {
        return picKoshin;
    }

    public String getPicChang() {
        return picChang;
    }

    public String getPdfKoshin() {
        return pdfKoshin;
    }

    public String getPdfChang() {
        return pdfChang;
    }

    public String getUrlKoshin() {
        return urlKoshin;
    }

    public String getUrlChang() {
        return urlChang;
    }

    public String getTempKoshin() {
        return tempKoshin;
    }

    public String getTempChang() {
        return tempChang;
    }

    public String getChangSerial() {
        return ChangSerial;
    }

    public String getKoshinSerial() {
        return KoshinSerial;
    }

    public void setCodeKoshin(String codeKoshin) {
        this.codeKoshin = codeKoshin;
    }

    public void setCodeChang(String codeChang) {
        this.codeChang = codeChang;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSizeChang(String sizeChang) {
        this.sizeChang = sizeChang;
    }

    public void setSizeKoshin(String sizeKoshin) {
        this.sizeKoshin = sizeKoshin;
    }

    public String getCodeKoshin() {
        return codeKoshin;
    }

    public String getCodeChang() {
        return codeChang;
    }

    public String getDescription() {
        return description;
    }

    public String getSizeChang() {
        return sizeChang;
    }

    public String getSizeKoshin() {
        return sizeKoshin;
    }

    public String getSpeciesKoshin() {
        return speciesKoshin;
    }

    public String getSpeciesChang() {
        return speciesChang;
    }

    public void speciesToKoshinAndChang(){

        String delimeter = "\\|";

        String curentSpecies = getSpecies();

        String[] species = curentSpecies.split(delimeter);

        if( curentSpecies == null){

            speciesChang = "";

            speciesKoshin = "";

            return;
        }


        if ( species.length != 1) {
            speciesChang = species[0];

            speciesKoshin = species[1];
        }else {

            speciesChang = curentSpecies;

            speciesKoshin = curentSpecies;

        }
    }

    public void vendorCodeToKoshinAndChang(){

        String delimeter = " ";

        String curentVendorCode = vendorCode;

        String[] codes = curentVendorCode.split(delimeter);

        codeChang = codes[0] + " " + codes[1] + " " + ChangSerial;

        codeKoshin = codes[0] + " " + codes[1] + " " + KoshinSerial;

    }

    public void serialToKoshinAndChang(){

        String delimeter = "\\|";

        String curentSerial = getSerial();

        String[] serials = curentSerial.split(delimeter);

        switch (serials.length){

            case 2:
                ChangSerial = serials[0];

                KoshinSerial = serials[1];

                break;

            case 3:

                ChangSerial = serials[0];

                KoshinSerial = serials[1];

                anotherSerial = serials[2];

                break;

            case 4:

                ChangSerial = serials[0];

                KoshinSerial = serials[1];

                anotherSerial = serials[2];

                anotherSerial1 = serials[3];

                break;

                default:

                    System.out.println("Error");

                    break;

        }

    }

    public void pdfToKoshinAndChang(){

        String delimeter = " ";

        String curentPdf = getPdf();

        String[] pdfs = curentPdf.split(delimeter);

        if( pdfs.length == 1){
            System.out.println("Error in url\n");
            return;
        }

        pdfChang = pdfs[1];

        pdfKoshin = pdfs[2];

    }

    public void picToKoshinAndChang(){

        String delimeter = " ";

        String curentPic = getPic();

        String[] pics = curentPic.split(delimeter);

        if( pics.length == 2){
            System.out.println("Error in url\n");
            return;
        }

        picChang = pics[1];

        picKoshin = pics[2];

    }

    public void urlToKoshinAndChang(){

        String delimeter = "multi";

        String curentUrl = getUrl();

        String[] urls = curentUrl.split(delimeter);

        if( urls.length == 1){
            System.out.println("Error in url\n");
            return;
        }


        urlKoshin = "koshin" + urls[1] + "-" + KoshinSerial.toLowerCase();

        urlChang = "chang" + urls[1] + "-" + ChangSerial.toLowerCase();

    }

    public void tempToKoshinAndChang(){

        String delimeter = "\\|";

        String curentTemp = getTemp();

        String[] temps = curentTemp.split(delimeter);

        if( temps.length == 1){

            tempChang = curentTemp;

            tempKoshin = curentTemp;

        }else {

            tempChang = temps[0];

            tempKoshin = temps[1];

        }

        }

    public void sizeToChangAndKoshin(){

        String delimeter = "\\|";

        String curentSize = getSize();

        String[] sizes = curentSize.split(delimeter);

        if( sizes.length == 1){

            sizeChang = curentSize;

            sizeKoshin = curentSize;

        }else {

            sizeChang = sizes[0];

            sizeKoshin = sizes[1];

        }

    }

    public void descriptionMapper(){

        String[] delimeter = {"Серии", ", серии"};

        String delProblem = ". .";

        String curentDescription = getDescription();

        if (curentDescription.contains(delimeter[0])){

            String[] descriptions = curentDescription.split(delimeter[0]);

            setDescription(descriptions[0] + ". Характеристики других номиналов серии отражены в таблице PDF.");

        }
        else if (curentDescription.contains(delimeter[1])){

            String[] descriptions = curentDescription.split(delimeter[1]);

            setDescription(descriptions[0] + ". Характеристики других номиналов серии отражены в таблице PDF.");

        }

        if (getDescription().contains(delProblem)){

            String[] descriptions = curentDescription.split("\\. \\.");

            setDescription(descriptions[0] + ". " + "Характеристики других номиналов серии отражены в таблице PDF.");

        }

    }

    public void setterHills(List<String> line){

        setVendorCode(line.get(0));
        description = line.get(1);
        setPic(line.get(2));
        setPdf(line.get(3));
        setUrl(line.get(4));
        setVendor(line.get(7));
        setSpecies(line.get(8));
        setCapacity(line.get(9));
        setVoltage(line.get(10));
        setSerial(line.get(11));
        setSize(line.get(12));
        setTemp(line.get(13));
        setTime(line.get(14));
        setCountOfOns(line.get(15));

    }



    public void mainBody(){

        descriptionMapper();

        pdfToKoshinAndChang();

        picToKoshinAndChang();

        serialToKoshinAndChang();

        urlToKoshinAndChang();

        vendorCodeToKoshinAndChang();

        tempToKoshinAndChang();

        sizeToChangAndKoshin();

        speciesToKoshinAndChang();

    }

}
