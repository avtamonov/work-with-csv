package com.company;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        Csv.Reader reader = new Csv.Reader( new FileReader("/home/arseny/multi.csv")).delimiter(';');

        Csv.Writer writer = new Csv.Writer( new FileWriter("/home/arseny/result.csv")).delimiter(';');

        Entity entity = new Entity();

        List<String> line;

        int i = 1;
        while ( i < 3442 ) {

            i++;

            line = reader.readLine();

            System.out.println(i);

            entity.setterHills(line);

            entity.mainBody();

            writer.value(entity.getCodeChang()).value(entity.getDescription()).value(entity.getPicChang()).value(entity.getPdfChang()).
                    value(entity.getUrlChang()).value("").value("").value("CHANG").value(entity.getSpeciesChang()).value(entity.getVoltage()).value(entity.getCapacity()).value(entity.getChangSeria())
                    .value(entity.getSizeChang()).value(entity.getTempChang()).value(entity.getTime()).newLine();

            writer.value(entity.getCodeKoshin()).value(entity.getDescription()).value(entity.getPicKoshin()).value(entity.getPdfKoshin()).
                    value(entity.getUrlKoshin()).value("").value("").value("KOSHIN").value(entity.getSpeciesKoshin()).value(entity.getVoltage()).value(entity.getCapacity()).value(entity.getKoshinSeria())
                    .value(entity.getSizeKoshin()).value(entity.getTempKoshin()).value(entity.getTime()).newLine();
        }

        writer.close();
        reader.close();
    }
}
